Formation python au sein de la DREES:

Sommaire:
* Introduction au langage
* Webscraping
* Manipulation de données en ```pandas```
* Modélisation en ```scikit-learn```

Instalations nécessaires: [suivre ce tutoriel](https://gitlab.com/DREES/tutoriels/blob/master/tutos/INSTALLATION_FORMATION_PYTHON.md)

Pour récupérer ce projet deux solutions:
* Via git
 * Lancer la commande ```git clone https://gitlab.com/DREES_code/OUTILS/python-initiation.git``` depuis "cmd" ou "Git Bash", trouvable dans le "Menu Démarrer"
* Sur le disque partagé I: à l'adresse suivante: ```I:\ECHANGE\NAOUR, Julien\python-initiation```

Pour lancer la formation via Jupyter:
* Ouvrer un Explorateur de fichier windows
* Aller dans le dossier "python-initiation" récupéré précédemment
* Faire un clic droit puis choisir l'option "Git Bash Here"
* Lancer la commande ```jupyter notebook``` dans l'invite de commande qui vient de s'ouvrir